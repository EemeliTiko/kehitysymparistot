import java.io.*;
import java.util.*; 
public class TehtavaII3 {
public static void main (String [ ] args) { 
int kuukausi ;

	Scanner lukija;
	lukija = new Scanner(System.in);
	
	System.out.println("Anna numero valilta 1-12.");
	kuukausi = lukija.nextInt () ;
	//aika samankaltainen tehtävä kuin 2 mutta käytetään enemmän casejä ja breakejä


switch (kuukausi) {

case 1 :
System.out.println("Tammikuu") ;
break ;
case 2 :
System.out.println("Helmikuu") ;
break ;
case 3 :
System.out.println("Maaliskuu") ;
break ;
case 4 :
System.out.println("Huuhtikuu") ;
break ;
case 5 :
System.out.println("Toukokuu") ;
break ;
case 6 :
System.out.println("Kesäkuu") ;
break ;
case 7 :
System.out.println("Heinäkuu") ;
break ;
case 8 :
System.out.println("Elokuu") ;
break ;
case 9 :
System.out.println("Syyskuu") ;
break ;
case 10 :
System.out.println("Lokakuu") ;
break ;
case 11 :
System.out.println("Marraskuu") ;
break ;
case 12 :
System.out.println("Joulukuu") ;
break ;
default :
System.out.println(kuukausi + " on virheellinen luku anna numero valilta 1-12.");
//eli jatketaan samalla tavalla ainoastaan vaan se, että defaulti laitetaan loppuun ja jos joku laittaa isomman tai
//pienemmän luvun kuin 1-12 niin se sanoo että luku on virheellinen muuten se ilmoittaa oikean kuukauden nimen.

}
}
}